package net.iescierva.ajfp.mislugareskotlin2021.datos

import android.app.Application
import android.content.res.Configuration
import net.iescierva.ajfp.mislugareskotlin2021.modelo.RepositorioLugares
import net.iescierva.ajfp.mislugareskotlin2021.modelo.LugaresLista
import net.iescierva.ajfp.mislugareskotlin2021.presentacion.AdaptadorLugares

class CustomApplication : Application() {
    val lugares: RepositorioLugares = LugaresLista()
    var adaptador = AdaptadorLugares(lugares)

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    override fun onCreate() {
        super.onCreate()
        // Required initialization logic here!
    }

    // Called by the system when the device configuration changes while your component is running.
    // Overriding this method is totally optional!
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }

    // This is called when the overall system is running low on memory,
    // and would like actively running processes to tighten their belts.
    // Overriding this method is totally optional!
    override fun onLowMemory() {
        super.onLowMemory()
    }
}