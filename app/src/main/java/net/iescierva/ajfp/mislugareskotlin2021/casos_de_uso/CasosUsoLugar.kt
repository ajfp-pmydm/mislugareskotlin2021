package net.iescierva.ajfp.mislugareskotlin2021.casos_de_uso

import android.app.Activity
import android.app.AlertDialog
import android.content.*
import net.iescierva.ajfp.mislugareskotlin2021.modelo.RepositorioLugares
import net.iescierva.ajfp.mislugareskotlin2021.presentacion.VistaLugarActivity
import net.iescierva.ajfp.mislugareskotlin2021.modelo.Lugar
import net.iescierva.ajfp.mislugareskotlin2021.presentacion.EdicionLugarActivity
import net.iescierva.ajfp.mislugareskotlin2021.modelo.GeoPunto
import android.os.Environment
import android.os.Build
import android.provider.MediaStore
import android.widget.Toast
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.widget.ImageView
import androidx.core.content.FileProvider
import net.iescierva.ajfp.mislugareskotlin2021.R
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.net.URL


class CasosUsoLugar(private val actividad: Activity, private val lugares: RepositorioLugares) {
    fun mostrar(pos: Int) {
        if (pos > lugares.size() - 1) {
            AlertDialog.Builder(actividad)
                .setTitle("\t\tError")
                .setIcon(R.drawable.alert)
                .setMessage(
                    """

Ese lugar no existe. Sólo hay ${lugares.size() - 1} lugares guardados."""
                )
                .setPositiveButton("Volver", null)
                .show()
            return
        }
        val i = Intent(actividad, VistaLugarActivity::class.java)
        i.putExtra("pos", pos)
        actividad.startActivity(i)
    }

    fun guardar(id: Int, nuevoLugar: Lugar?) {
        if (nuevoLugar != null) {
            lugares.update_element(id, nuevoLugar)
        }
    }

    fun editar(pos: Int, codigoSolicitud: Int) {
        val i = Intent(actividad, EdicionLugarActivity::class.java)
        i.putExtra("pos", pos)
        actividad.startActivityForResult(i, codigoSolicitud)
    }

    //Cómo obtener los extras:
    fun borrarLugar(id: Int) {
        AlertDialog.Builder(actividad)
            .setTitle("Borrado de lugar")
            .setMessage("¿Estás seguro de que quieres eliminar este lugar?")
            .setPositiveButton("Confirmar") { dialog: DialogInterface?, whichButton: Int ->
                lugares.delete(id)
                actividad.finish()
            }
            .setNegativeButton("Cancelar", null)
            .show()
    }

    // INTENCIONES
    fun compartir(lugar: Lugar) {
        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        i.putExtra(
            Intent.EXTRA_TEXT,
            lugar.nombre + " - " + lugar.url
        )
        actividad.startActivity(i)
    }

    fun llamarTelefono(lugar: Lugar) {
        actividad.startActivity(
            Intent(
                Intent.ACTION_DIAL,
                Uri.parse("tel:" + lugar.telefono)
            )
        )
    }

    fun verPgWeb(lugar: Lugar) {
        actividad.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(lugar.url)
            )
        )
    }

    fun verMapa(lugar: Lugar) {
        val lat = lugar.posicion.latitud
        val lon = lugar.posicion.longitud
        val uri = if (lugar.posicion !== GeoPunto(
                lat,
                lon
            )
        ) Uri.parse("geo:$lat,$lon") else Uri.parse("geo:0,0?q=" + lugar.direccion)
        actividad.startActivity(Intent("android.intent.action.VIEW", uri))
    }

    // FOTOGRAFÍAS
    fun tomarFoto(codigoSolicitud: Int): Uri? {
        return try {
            val uriUltimaFoto: Uri
            val file = File.createTempFile(
                "img_" + System.currentTimeMillis() / 1000, ".jpg",
                actividad.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            )
            uriUltimaFoto = if (Build.VERSION.SDK_INT >= 24) {
                FileProvider.getUriForFile(
                    actividad, "net.iescierva.ajfp.mislugareskotlin2021.fileProvider", file
                )
            } else {
                Uri.fromFile(file)
            }
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriUltimaFoto)
            actividad.startActivityForResult(intent, codigoSolicitud)
            uriUltimaFoto
        } catch (ex: IOException) {
            Toast.makeText(
                actividad, "Error al crear fichero de imagen",
                Toast.LENGTH_LONG
            ).show()
            null
        }
    }

    fun galeria(resultadoGaleria: Int) {
        val action: String
        action = if (Build.VERSION.SDK_INT >= 19) { // API 19 - Kitkat
            Intent.ACTION_OPEN_DOCUMENT
        } else {
            Intent.ACTION_PICK
        }
        val intent = Intent(
            action,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        actividad.startActivityForResult(intent, resultadoGaleria)
    }

    fun ponerFoto(pos: Int, uri: String?, imageView: ImageView) {
        val lugar = lugares.get_element(pos)
        lugar.foto = uri
        visualizarFoto(lugar, imageView)
    }

    fun visualizarFoto(lugar: Lugar, imageView: ImageView) {
        val foto = lugar.foto
        if (foto != null && !foto.isEmpty()) imageView.setImageBitmap(
            reduceBitmap(
                actividad,
                lugar.foto!!,
                1024,
                1024
            )
        ) else imageView.setImageResource(R.drawable.add_photo)
    }

    private fun reduceBitmap(contexto: Context, uri: String, maxAncho: Int, maxAlto: Int): Bitmap? {
        return try {
            var input: InputStream? = null
            val u = Uri.parse(uri)
            if (u.scheme == "http" || u.scheme == "https") {
                input = URL(uri).openStream()
            } else {
                val c: ContentResolver
                c = contexto.contentResolver
                try {
                    c.takePersistableUriPermission(u, Intent.FLAG_GRANT_READ_URI_PERMISSION)
                } catch (e: Exception) {
                }
                input = c.openInputStream(u)
            }
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            options.inSampleSize = Math.max(
                Math.ceil((options.outWidth / maxAncho).toDouble()),
                Math.ceil((options.outHeight / maxAlto).toDouble())
            ).toInt()
            options.inJustDecodeBounds = false
            BitmapFactory.decodeStream(input, null, options)
        } catch (e: FileNotFoundException) {
            Toast.makeText(
                contexto, "Fichero/recurso de imagen no encontrado",
                Toast.LENGTH_LONG
            ).show()
            e.printStackTrace()
            null
        } catch (e: IOException) {
            Toast.makeText(
                contexto, "Error accediendo a imagen",
                Toast.LENGTH_LONG
            ).show()
            e.printStackTrace()
            null
        }
    }
}