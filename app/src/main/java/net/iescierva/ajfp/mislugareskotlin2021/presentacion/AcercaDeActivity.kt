package net.iescierva.ajfp.mislugareskotlin2021.presentacion


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import net.iescierva.ajfp.mislugareskotlin2021.R

//Extender AppCompatActivity es necesario para utilizar la Toolbar
class AcercaDeActivity : AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acercade)
    }
}