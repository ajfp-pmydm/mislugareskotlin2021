package net.iescierva.ajfp.mislugareskotlin2021.presentacion

import android.os.Bundle
import android.preference.PreferenceFragment
import net.iescierva.ajfp.mislugareskotlin2021.R


class PreferenciasFragment : PreferenceFragment() {
    //Para resumir la actividad queda guardado el estado en forma de paquete
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferencias)
    }
}