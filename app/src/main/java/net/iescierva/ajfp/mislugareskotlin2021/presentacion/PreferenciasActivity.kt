package net.iescierva.ajfp.mislugareskotlin2021.presentacion

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


class PreferenciasActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //El FragmentManager utiliza una transacción para introducir y ordenar los fragmentos
        fragmentManager.beginTransaction()
            .replace(android.R.id.content, PreferenciasFragment()) //.replace(new Fragment())
            .commit()
    }
}