package net.iescierva.ajfp.mislugareskotlin2021.casos_de_uso

import android.app.Activity
import android.content.*
import android.widget.Toast
import net.iescierva.ajfp.mislugareskotlin2021.presentacion.AcercaDeActivity
import android.widget.EditText
import android.preference.PreferenceManager
import androidx.appcompat.app.AlertDialog
import net.iescierva.ajfp.mislugareskotlin2021.presentacion.PreferenciasActivity

object CasosUsoActividades {
    //El contexto es desde donde se abre
    @JvmStatic
    fun lanzarAcercaDe(context: Context) {
        val i = Intent(context, AcercaDeActivity::class.java)
        context.startActivity(i)
    }

    @JvmStatic
    fun lanzarPreferencias(context: Context) {
        val i = Intent(context, PreferenciasActivity::class.java)
        context.startActivity(i)
    }

    @JvmStatic
    fun lanzarVistaLugar(usosLugar: CasosUsoLugar, context: Context?) {
        val entrada = EditText(context)
        entrada.setText("0")
        AlertDialog.Builder(context!!)
            .setTitle("Selección de lugar")
            .setMessage("indica su id:")
            .setView(entrada)
            .setPositiveButton("Ok") { dialog: DialogInterface?, whichButton: Int ->
                val id = entrada.text.toString().toInt()
                usosLugar.mostrar(id)
            }
            .setNegativeButton("Cancelar", null)
            .show()
    }

    fun mostrarPreferencias(context: Context?) {
        //Devuelve el diccionario de las preferencias que hemos definido en preferencias.xml
        val pref = PreferenceManager.getDefaultSharedPreferences(context)
        val s = String.format(
            "%-50s %s %-27s %s %-47s %s %-41s %s %-52s %s",
            "Notificaciones: \t",
            pref.getBoolean("notificaciones", true),
            "\nE-mail: \t",
            pref.getString("email", ""),
            "\nTipos de Notificaciones: \t",
            pref.getString("tiposNotificaciones", "0"),
            "\nMáximo de lugares a mostrar: \t",
            pref.getString("maximo", "?"),
            "\nOrden de los lugares: \t",
            pref.getString("orden", "")
        )
        Toast.makeText(context, s, Toast.LENGTH_LONG).show()
    }

    fun salir(context: Activity) {
        context.finish()
    }
}