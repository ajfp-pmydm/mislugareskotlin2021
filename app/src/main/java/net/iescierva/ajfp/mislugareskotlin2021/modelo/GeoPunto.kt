package net.iescierva.ajfp.mislugareskotlin2021.modelo

import java.util.*


//Mirar como crear kotlindocs y la clase GeoPunto
class GeoPunto
/**
 * @param latitud latitud del punto en forma (+:Norte, -:Sur)
 * @param longitud longitud del punto en forma (+:Este, -:Oeste)
 */(var latitud: Double, var longitud: Double) {
    override fun toString(): String {
        return "GeoPunto{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                '}'
    }

    /**
     * @param punto Objeto GeoPunto usado para calcular la distancia a otro punto geográfico.
     * @return La distancia en metros.
     */
    fun distancia(punto: GeoPunto): Double {
        val RADIO_TIERRA = 6371000.0 // en metros
        val dLat = Math.toRadians(latitud - punto.latitud)
        val dLon = Math.toRadians(longitud - punto.longitud)
        val lat1 = Math.toRadians(punto.latitud)
        val lat2 = Math.toRadians(latitud)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) *
                Math.cos(lat1) * Math.cos(lat2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return c * RADIO_TIERRA
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is GeoPunto) return false
        val geoPunto = o
        return java.lang.Double.compare(geoPunto.latitud, latitud) == 0 && java.lang.Double.compare(
            geoPunto.longitud,
            longitud
        ) == 0
    }

    override fun hashCode(): Int {
        return Objects.hash(latitud, longitud)
    }

    companion object {
        var SIN_POSICION = GeoPunto(0.0, 0.0)
    }
}