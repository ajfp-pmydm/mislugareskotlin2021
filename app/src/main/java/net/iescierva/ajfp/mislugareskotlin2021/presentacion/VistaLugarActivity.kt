package net.iescierva.ajfp.mislugareskotlin2021.presentacion

import android.content.Intent
import android.icu.text.DateFormat
import android.net.Uri
import android.os.Bundle

import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import net.iescierva.ajfp.mislugareskotlin2021.R
import net.iescierva.ajfp.mislugareskotlin2021.casos_de_uso.CasosUsoLugar
import net.iescierva.ajfp.mislugareskotlin2021.datos.CustomApplication
import net.iescierva.ajfp.mislugareskotlin2021.modelo.Lugar
import net.iescierva.ajfp.mislugareskotlin2021.modelo.RepositorioLugares
import java.util.*


class VistaLugarActivity : AppCompatActivity() {
    private var foto: ImageView? = null
    private var lugares: RepositorioLugares? = null
    private var usosLugar: CasosUsoLugar? = null
    private var pos = 0
    private var lugar: Lugar? = null
    private var uriUltimaFoto: Uri? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vista_lugar)
        val extras = intent.extras
        pos = extras!!.getInt("pos", 0)
        lugares = (application as CustomApplication).lugares
        usosLugar = CasosUsoLugar(this, lugares!!)
        lugar = lugares!!.get_element(pos)
        foto = findViewById(R.id.foto)
        actualizaVistas()
    }

    fun galeria(view: View?) {
        usosLugar!!.galeria(RESULTADO_GALERIA)
    }

    private fun checkInfoDisponible() {
        val layoutTelef = findViewById<LinearLayout>(R.id.layoutTelef)
        val layoutComent = findViewById<LinearLayout>(R.id.layoutComent)
        val layoutURL = findViewById<LinearLayout>(R.id.layoutURL)
        val layoutDirecc = findViewById<LinearLayout>(R.id.layoutDirecc)
        if (lugar!!.telefono == 0) {
            layoutTelef.visibility = View.GONE
        } else {
            layoutTelef.visibility = View.VISIBLE
            val vistaTelef = findViewById<TextView>(R.id.telefonoVista)
            vistaTelef.text = Integer.toString(lugar!!.telefono)
        }
        if (lugar!!.comentario == "") {
            layoutComent.visibility = View.GONE
        } else {
            layoutComent.visibility = View.VISIBLE
            val vistaComent = findViewById<TextView>(R.id.comentarioVista)
            vistaComent.text = lugar!!.comentario
        }
        if (lugar!!.url == "") {
            layoutURL.visibility = View.GONE
        } else {
            layoutURL.visibility = View.VISIBLE
            val vistaURL = findViewById<TextView>(R.id.urlVista)
            vistaURL.text = lugar!!.url
        }
        if (lugar!!.direccion == "") {
            layoutDirecc.visibility = View.GONE
        } else {
            layoutDirecc.visibility = View.VISIBLE
            val vistaDirecc = findViewById<TextView>(R.id.direccionVista)
            vistaDirecc.text = lugar!!.direccion
        }
    }

    fun actualizaVistas() {
        val nombre = findViewById<TextView>(R.id.nombreLugar)
        val logo_tipo = findViewById<ImageView>(R.id.logo_tipo)
        val tipo = findViewById<TextView>(R.id.tipo)
        val fecha = findViewById<TextView>(R.id.fecha)
        val hora = findViewById<TextView>(R.id.hora)
        val valoracion = findViewById<RatingBar>(R.id.valoracion)
        nombre.text = lugar!!.nombre
        logo_tipo.setImageResource(lugar!!.tipo.recurso)
        tipo.text = lugar!!.tipo.texto
        fecha.text = DateFormat.getDateInstance().format(
            Date(lugar!!.fecha)
        )
        hora.text = DateFormat.getTimeInstance().format(
            Date(lugar!!.fecha)
        )
        valoracion.rating = lugar!!.valoracion
        valoracion.onRatingBarChangeListener =
            RatingBar.OnRatingBarChangeListener { ratingBar: RatingBar?, valor: Float, fromUser: Boolean ->
                lugar!!.valoracion = valor
            }
        checkInfoDisponible()
        usosLugar!!.visualizarFoto(lugar!!, foto!!)
    }

    fun tomarFoto(view: View?) {
        uriUltimaFoto = usosLugar!!.tomarFoto(RESULTADO_FOTO)
    }

    fun eliminarFoto(view: View?) {
        usosLugar!!.ponerFoto(pos, "", foto!!)
        foto!!.setImageResource(R.drawable.add_photo)
    }

    fun verMapa(view: View?) {
        usosLugar!!.verMapa(lugar!!)
    }

    fun llamarTelefono(view: View?) {
        usosLugar!!.llamarTelefono(lugar!!)
    }

    fun verPgWeb(view: View?) {
        usosLugar!!.verPgWeb(lugar!!)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.vista_lugar_menu, menu)
        return true
    }

    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULTADO_EDITAR) {
            actualizaVistas()
            findViewById<View>(R.id.scrollView1).invalidate()
        } else if (requestCode == RESULTADO_GALERIA) {
            if (resultCode == RESULT_OK) {
                usosLugar!!.ponerFoto(pos, data!!.dataString, foto!!)
            } else {
                Toast.makeText(this, "Foto no cargada", Toast.LENGTH_LONG).show()
            }
        } else if (requestCode == RESULTADO_FOTO) {
            if (resultCode == RESULT_OK && uriUltimaFoto != null) {
                lugar!!.foto = uriUltimaFoto.toString()
                usosLugar!!.ponerFoto(pos, lugar!!.foto, foto!!)
            } else {
                Toast.makeText(this, "Error en captura", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.accion_compartir -> {
                usosLugar!!.compartir(lugar!!)
                usosLugar!!.verMapa(lugar!!)
                usosLugar!!.editar(pos, 1)
                true
            }
            R.id.accion_llegar -> {
                usosLugar!!.verMapa(lugar!!)
                usosLugar!!.editar(pos, 1)
                true
            }
            R.id.accion_editar -> {
                usosLugar!!.editar(pos, 1)
                true
            }
            R.id.accion_borrar -> {
                usosLugar!!.borrarLugar(pos)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val RESULTADO_GALERIA = 2
        const val RESULTADO_FOTO = 3
        const val RESULTADO_EDITAR = 1
    }
}