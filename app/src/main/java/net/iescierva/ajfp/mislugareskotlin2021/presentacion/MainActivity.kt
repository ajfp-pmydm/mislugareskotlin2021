package net.iescierva.ajfp.mislugareskotlin2021.presentacion


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.iescierva.ajfp.mislugareskotlin2021.R
import net.iescierva.ajfp.mislugareskotlin2021.casos_de_uso.CasosUsoActividades
import net.iescierva.ajfp.mislugareskotlin2021.casos_de_uso.CasosUsoLugar
import net.iescierva.ajfp.mislugareskotlin2021.databinding.ActivityMainBinding
import net.iescierva.ajfp.mislugareskotlin2021.datos.CustomApplication
import net.iescierva.ajfp.mislugareskotlin2021.modelo.RepositorioLugares

class MainActivity : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    var adaptador: AdaptadorLugares? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var separador: RecyclerView.ItemDecoration? = null
    private var usosLugar: CasosUsoLugar? = null
    private var lugares: RepositorioLugares? = null
    val PERMISSION_ALL = 1
    var PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(
            layoutInflater
        )
        //Nos devuelve siempre el layout de la actividad del binding: ActivityMain
        setContentView(binding.root)

        //Obtendremos los lugares lo primero ya que CustomApplication se ejecuta la primera
        lugares = (application as CustomApplication).lugares
        usosLugar = CasosUsoLugar(this, lugares!!)

        //Acceder a la vista "recycler_view" de la actividad actual (MainActivity)
        recyclerView = binding.includeContentMain.recyclerView
        crearListaLugares()
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL)
        }
    }

    fun crearListaLugares() {
        //Construir el adaptador y asignarlo al recyclerView
        adaptador = AdaptadorLugares(this, lugares)
        recyclerView!!.adapter = adaptador

        //Construir y asignar el LayoutManager, en este caso de tipo Linear
        layoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = layoutManager

        //Construir y asignar el separador de elementos
        separador = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        recyclerView!!.addItemDecoration(separador as DividerItemDecoration)
        adaptador!!.setOnItemClickListener { v: View? ->
            val pos = recyclerView!!.getChildAdapterPosition(v!!)
            usosLugar!!.mostrar(pos)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_settings) {
            //Le podemos pasar una vista si queremos que se llegue a la actividad con una vista
            CasosUsoActividades.lanzarPreferencias(this)
            return true
        }
        if (id == R.id.acercaDe) {
            CasosUsoActividades.lanzarAcercaDe(this)
            return true
        }
        if (id == R.id.menu_buscar) {
            CasosUsoActividades.lanzarVistaLugar(usosLugar!!, this)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    //Este método sólo devuelve true si todos los condicionales de permiso no aceptado son falsos
    private fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    context!!,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

}