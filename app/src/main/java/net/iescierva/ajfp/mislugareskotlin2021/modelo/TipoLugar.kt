package net.iescierva.ajfp.mislugareskotlin2021.modelo

import kotlin.jvm.JvmOverloads
import net.iescierva.ajfp.mislugareskotlin2021.modelo.TipoLugar
import net.iescierva.ajfp.mislugareskotlin2021.modelo.GeoPunto
import net.iescierva.ajfp.mislugareskotlin2021.R
import net.iescierva.ajfp.mislugareskotlin2021.modelo.RepositorioLugares
import net.iescierva.ajfp.mislugareskotlin2021.modelo.Lugar

enum class TipoLugar(val texto: String, val recurso: Int) {
    OTROS("Otros", R.drawable.otros), RESTAURANTE("Restaurante", R.drawable.restaurante), BAR(
        "Bar",
        R.drawable.bar
    ),
    COPAS("Copas", R.drawable.copas), ESPECTACULO(
        "Espectáculo",
        R.drawable.espectaculos
    ),
    HOTEL("Hotel", R.drawable.hotel), COMPRAS("Compras", R.drawable.compras), EDUCACION(
        "Educación",
        R.drawable.educacion
    ),
    DEPORTE("Deporte", R.drawable.deporte), NATURALEZA(
        "Naturaleza",
        R.drawable.naturaleza
    ),
    GASOLINERA("Gasolinera", R.drawable.gasolinera);

    override fun toString(): String {
        return "TipoLugar{" +
                "texto='" + texto + '\'' +
                ", recurso=" + recurso +
                '}'
    }

    companion object {
        @JvmStatic
        val nombres: Array<String?>
            get() {
                val resultado = arrayOfNulls<String>(values().size)
                for (tipo in values()) {
                    resultado[tipo.ordinal] = tipo.texto
                }
                return resultado
            }
    }
}