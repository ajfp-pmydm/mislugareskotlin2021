package net.iescierva.ajfp.mislugareskotlin2021.presentacion

import net.iescierva.ajfp.mislugareskotlin2021.modelo.TipoLugar.Companion.nombres
import androidx.appcompat.app.AppCompatActivity
import net.iescierva.ajfp.mislugareskotlin2021.casos_de_uso.CasosUsoLugar
import android.os.Bundle
import net.iescierva.ajfp.mislugareskotlin2021.datos.CustomApplication
import net.iescierva.ajfp.mislugareskotlin2021.modelo.Lugar
import net.iescierva.ajfp.mislugareskotlin2021.modelo.TipoLugar
import android.content.Intent
import android.net.Uri
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import net.iescierva.ajfp.mislugareskotlin2021.R
import net.iescierva.ajfp.mislugareskotlin2021.databinding.EdicionLugarBinding


class EdicionLugarActivity : AppCompatActivity() {
    private var nombre: EditText? = null
    private var tipo: Spinner? = null
    private var direccion: EditText? = null
    private var telefono: EditText? = null
    private var url: EditText? = null
    private var comentario: EditText? = null
    private var pos = 0
    private var usosLugar: CasosUsoLugar? = null
    private var lugar: Lugar? = null
    var binding: EdicionLugarBinding? = null
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = EdicionLugarBinding.inflate(
            layoutInflater
        )
        setContentView(binding!!.root)
        nombre = binding!!.nombreLugar
        direccion = binding!!.direccion
        telefono = binding!!.telefono
        comentario = binding!!.edComentario
        url = binding!!.url
        val lugares = (application as CustomApplication).lugares

        //Debe iniciarse la actividad con el id pasado desde otra actividad
        pos = intent.extras!!.getInt("pos", 0)
        lugar = lugares.get_element(pos)
        usosLugar = CasosUsoLugar(this, lugares)
        // Asignamos el lugar antes para poder mostrar su tipo en el spinner
        crearSpinner()
        actualizaVistas()
    }

    private fun actualizaVistas() {
        nombre!!.setText(lugar!!.nombre)
        direccion!!.setText(lugar!!.direccion)
        telefono!!.setText(Integer.toString(lugar!!.telefono))
        comentario!!.setText(lugar!!.comentario)
        url!!.setText(lugar!!.url)
    }

    private fun crearSpinner() {
        tipo = binding!!.tipo
        val adaptador = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item, nombres
        )
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        tipo!!.adapter = adaptador
        tipo!!.setSelection(lugar!!.tipo.ordinal)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.edicion_lugar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == findViewById<View>(R.id.accion_guardar).id) {
            lugar!!.nombre = nombre!!.text.toString()
            lugar!!.tipo = TipoLugar.values()[tipo!!.selectedItemPosition]
            lugar!!.direccion = direccion!!.text.toString()
            lugar!!.telefono = telefono!!.text.toString().toInt()
            lugar!!.url = url!!.text.toString()
            lugar!!.comentario = comentario!!.text.toString()
            usosLugar!!.guardar(pos, lugar)
        } else if (item.itemId == R.id.accion_buscar) {
            val nombre = lugar!!.nombre
            val uri = Uri.parse("https://www.google.com/search?q=$nombre")
            val gSearchIntent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(gSearchIntent)
        }
        finish()
        return true
    }
}