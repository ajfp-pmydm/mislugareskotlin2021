package net.iescierva.ajfp.mislugareskotlin2021.modelo

import kotlin.jvm.JvmOverloads
import net.iescierva.ajfp.mislugareskotlin2021.modelo.TipoLugar
import net.iescierva.ajfp.mislugareskotlin2021.modelo.GeoPunto
import net.iescierva.ajfp.mislugareskotlin2021.R
import net.iescierva.ajfp.mislugareskotlin2021.modelo.RepositorioLugares
import net.iescierva.ajfp.mislugareskotlin2021.modelo.Lugar

class Lugar @JvmOverloads constructor(
    nombre: String = "",
    direccion: String = "",
    latitud: Double = 0.0,
    longitud: Double = 0.0,
    foto: String? = "",
    tipo: TipoLugar = TipoLugar.OTROS,
    telefono: Int = 0,
    url: String = "",
    comentario: String = "",
    valoracion: Int = 0
) {
    var nombre //Añadido
            : String
    var direccion //Añadido
            : String
    var posicion: GeoPunto
    var tipo //Añadido
            : TipoLugar
    var foto: String?
    var telefono //Añadido
            : Int
    var url //Añadido
            : String
    var comentario //Añadido
            : String
    var fecha //Añadido
            : Long
    var valoracion //Añadido
            : Float

    //constructor alternativo recibiendo GeoPunto en lugar de latitud y longitud
    constructor(
        nombre: String,
        direccion: String,
        p: GeoPunto,
        foto: String?,
        tipo: TipoLugar,
        telefono: Int,
        url: String,
        comentario: String,
        valoracion: Int
    ) : this(
        nombre, direccion, p.latitud, p.longitud,
        foto, tipo, telefono, url, comentario, valoracion
    ) {
    }

    override fun toString(): String {
        return "Lugar{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", posicion=" + posicion +
                ", tipo=" + tipo +
                ", telefono=" + telefono +
                ", url='" + url + '\'' +
                ", comentario='" + comentario + '\'' +
                ", fecha=" + fecha +
                ", valoracion=" + valoracion +
                '}'
    }

    //Casteo de String a Enum es TipoLugar.valueOf(String). La valoración y la foto no están añadidas en el formulario
    init {
        fecha = System.currentTimeMillis()
        posicion = GeoPunto(latitud, longitud)
        this.foto = foto
        this.tipo = tipo
        this.nombre = nombre
        this.direccion = direccion
        this.telefono = telefono
        this.url = url
        this.comentario = comentario
        this.valoracion = valoracion.toFloat()
    }
}